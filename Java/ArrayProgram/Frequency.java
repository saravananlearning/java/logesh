package ArrayProgram;

class Frequency {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 8, 3, 2, 2, 2, 5, 1};
        int[] freq = new int[arr.length];

        for(int i=0; i<arr.length; i++) {
            if(arr[i] != 0) {
                freq[i]++;
                for(int j=i+1; j<arr.length; j++) {
                    if(arr[i] == arr[j]) {
                        freq[i]++;
                        arr[j] = 0;
                    }
                }
            }
        }

        for(int i=0; i<arr.length; i++) {
            System.out.println(arr[i] + " -> " + freq[i]);
        }
    }
}

/* Will work later */
class Frequency2 {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 8, 3, 2, 2, 2, 5, 1};
        int[] freq = new int[arr.length];

        for(int i=0; i<arr.length; i++) {
            if(arr[i] != 0) {
                freq[i]++;
                for(int j=i+1; j<arr.length; j++) {
                    if(arr[i] == arr[j]) {
                        freq[i]++;
                        arr[j] = 0;
                    }
                }
            }
        }

        for(int i=0; i<arr.length; i++) {
            System.out.println(arr[i] + " -> " + freq[i]);
        }
    }
}

