package ArrayProgram;

import java.util.Scanner;

class Sorting {
    public static void main(String[] args) {
        System.out.println("Sorting");

        System.out.println("Enter the number of element in the array");
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();

        int[] arr = new int[length];

        System.out.println("Enter the element in the array");
        for(int i=0; i< length; i++) {
            arr[i] = scanner.nextInt();
        }

        for(int i=0; i<length; i++) {
            for(int j=i+1; j<length; j++) {
                if(arr[i] > arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        for(int i=0; i<length; i++) {
            System.out.print(arr[i] + " ");
        }

        scanner.close();
    }    
}
