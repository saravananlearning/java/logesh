class Switch {
    public static void main(String[] args) {
        System.out.println("Switch Statement");

        int age = 65;
        switch(age) {
            case 10:
                System.out.println("You are not eligible to vote");
                break;
            case 18:
                System.out.println("You are eligible to vote");
                break;
            case 65:
                System.out.println("You are senior citizen");
                break;
            default:
                System.out.println("please give the valid age");
                break;
        }
    }
}