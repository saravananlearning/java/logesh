class IfElse {
    public static void main(String[] args) {
        System.out.println("If else");
        
        boolean isHuman = true;
        if(isHuman) {
            System.out.println("Human being");
        } else {
            System.out.println("Animal");
        }
    }
}
