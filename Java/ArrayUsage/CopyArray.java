package ArrayUsage;

import java.util.Arrays;

class CopyArray {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3, 4, 5};

        int[] copyArr1 = new int[arr.length];
        System.out.println("No of element based on the size with intial 0 value: " + Arrays.toString(copyArr1));

        // Copy all the element of array into another array

        for(int i=0; i<arr.length; i++) {
            copyArr1[i] = arr[i];
        }

        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(copyArr1));
    }

}
