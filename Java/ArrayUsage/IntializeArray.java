package ArrayUsage;

import java.util.Arrays;

class IntializeArray {
    public static void main(String[] args) {
        int[] arr1 = new int[]{1, 2, 3, 4, 5};

        System.out.println(Arrays.toString(arr1));
    }
}
