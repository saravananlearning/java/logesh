import java.util.Scanner;

class ScannerUsage {
    public static void main(String[] args) {
        System.out.println("Scanner in Java");

        Scanner scanner = new Scanner(System.in);
        // System.out.println("Enter a number");
        // int number = scanner.nextInt();
        // System.out.println(number);

        System.out.println("Enter a string");
        String string = scanner.nextLine();
        System.out.println(string);

        System.out.println("Enter a character");
        char character = scanner.nextLine().charAt(0);
        System.out.println(character);
        scanner.close();
    }
}
