package ObjectUsage;

class Student {
    private int regNo;
    private String name;
    private char gender;
    private int age;
    private boolean isFeePaid;
    
    public Student(int regNo, String name, char gender, int age, boolean isFeePaid) {
        System.out.println("Before Intialization -> " + this.regNo + " " + this.name + " " + this.gender + " " + this.age + " " + this.isFeePaid);
        this.regNo = regNo;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.isFeePaid = isFeePaid;
        System.out.println("After Intialization -> " + this.regNo + " " + this.name + " " + this.gender + " " + this.age + " " + this.isFeePaid);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }

        if(obj == this) {
            return true;
        }

        return this.getRegNo() == ((Student) obj).getRegNo();
    }

    public int getRegNo() {
        return regNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getIsFeePaid() {
        return isFeePaid;
    }

    public void setIsFeePaid(boolean isFeePaid) {
        this.isFeePaid = isFeePaid;
    }

}
