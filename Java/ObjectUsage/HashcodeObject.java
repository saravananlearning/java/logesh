package ObjectUsage;

class HashcodeObject {
    public static void main(String[] args) {
        Student obj1 = new Student(1, "Logesh", 'M', 25, false);
        Student obj2 = new Student(1, "Logesh", 'M', 25, false);

        int obj1HashCode = obj1.hashCode();
        int obj2HashCode = obj2.hashCode();

        System.out.println("Hashcode of obj1 -> " + obj1HashCode);
        System.out.println("Hashcode of obj2 -> " + obj2HashCode);
        /**
         * By default it returns false
         * We will override the Object class's equals method in Student class
         * That's why it returns true
         */
        System.out.println("Comparing Objects using equals() -> " + obj1.equals(obj2));
        System.out.println("Comparing Objects using == -> " + (obj1 == obj2));
    }
}
