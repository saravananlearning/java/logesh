package ObjectConcept.ObjectIntialization.ByMethod;

class Student {
    int regNo;
    String name;

    void initializeTheValue(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    void displayTheValue() {
        System.out.println("Student reg no -> " + regNo);
        System.out.println("Student name -> " + name);
    }

}

class ByMethod {
    public static void main(String[] args) {
        Student student = new Student();
        student.displayTheValue();
        student.initializeTheValue(100, "Logesh");
        student.displayTheValue();
    }
}
