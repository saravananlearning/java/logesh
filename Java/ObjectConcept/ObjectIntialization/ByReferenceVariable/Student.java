package ObjectConcept.ObjectIntialization.ByReferenceVariable;

class Student {
    int regNo;
    String name;
}

class ByReferenceVariable {
    public static void main(String[] args) {
        Student student = new Student();
        System.out.println("Before Intialization");
        System.out.println("Student reg no -> " + student.regNo);
        System.out.println("Student name -> " + student.name);

        student.regNo = 100;
        student.name = "Logesh";
        System.out.println("After Intialization");

        System.out.println("Student reg no -> " + student.regNo);
        System.out.println("Student name -> " + student.name);
    }

}