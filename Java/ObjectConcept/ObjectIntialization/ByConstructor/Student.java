package ObjectConcept.ObjectIntialization.ByConstructor;

class Student {
    int regNo;
    String name;

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    void displayTheValue() {
        System.out.println("Student reg no -> " + regNo);
        System.out.println("Student name -> " + name);
    }
}

class ByConstructor {
    public static void main(String[] args) {
        Student student = new Student(100, "Logesh");
        student.displayTheValue();
    }
}
