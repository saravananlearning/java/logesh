package ObjectConcept.StaticKeyword.StaticVariable;

class Student {
    int regNo;
    String name;
    static String college = "MIT";

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    void displayTheValue() {
        System.out.println(regNo + " -> " + name + " -> " + college);
    }
}

class StaticVariable {
    public static void main(String[] args) {
        Student student1 = new Student(100, "Logesh");
        Student student2 = new Student(101, "Ranjith");

        student1.displayTheValue();
        student2.displayTheValue();
        Student.college = "Madras Institute of Technology";
        student1.displayTheValue();
        student2.displayTheValue();
    }
}
