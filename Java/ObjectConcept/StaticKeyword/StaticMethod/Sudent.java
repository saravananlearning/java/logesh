package ObjectConcept.StaticKeyword.StaticMethod;

class Student {
    int regNo;
    String name;
    static String college = "MIT";

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    // Can access static variable only
    static void changeTheStaticCollegeValue() {
        college = "Madras Institute of Technology";
        // regNo = 200; error - cann't access non-static variable
        // displayTheValue(); error - cann't access non-static method
        // this and super cannot be used in static context
    }

    void displayTheValue() {
        System.out.println(regNo + " -> " + name + " -> " + college);
    }
}

class StaticMethod {
    public static void main(String[] args) {
        Student student1 = new Student(100, "Logesh");
        Student student2 = new Student(101, "Ranjith");

        student1.displayTheValue();
        student2.displayTheValue();
        Student.changeTheStaticCollegeValue();
        student1.displayTheValue();
        student2.displayTheValue();
    }
}
