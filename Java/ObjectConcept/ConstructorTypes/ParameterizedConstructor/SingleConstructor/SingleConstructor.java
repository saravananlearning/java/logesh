package ObjectConcept.ConstructorTypes.ParameterizedConstructor.SingleConstructor;

class Student {
    int regNo;
    String name;

    // constructor is just like a method but without return type.

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    void displayTheValue() {
        System.out.println("Student reg no -> " + regNo);
        System.out.println("Student name -> " + name);
    }
}

class SingleConstructor {
    public static void main(String[] args) {
        Student student = new Student(100, "Logesh");
        student.displayTheValue();
    }
}
