package ObjectConcept.ConstructorTypes.ParameterizedConstructor.MultipleConstructor;


class Student {
    int regNo;
    String name;

    // ## Constructor overloading ## in Java is a technique of having more than one constructor with different parameter lists
    // They are differentiated by the compiler by the number of parameters in the list and their types

    Student(int regNo) {
        this.regNo = regNo;
    }

    Student(String name) {
        this.name = name;
    }

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    void displayTheValue() {
        System.out.println("Student reg no -> " + regNo);
        System.out.println("Student name -> " + name);
    }
}

class MultipleConstructor {
    public static void main(String[] args) {
        Student student1 = new Student(100);
        student1.displayTheValue();

        Student student2 = new Student("Logesh");
        student2.displayTheValue();

        Student student3 = new Student(100, "Logesh");
        student3.displayTheValue();
    }
}
