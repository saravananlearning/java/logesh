package ObjectConcept.ConstructorTypes.DefaultConstructor;

class Student {
    int regNo;
    String name;

    /*  If there is no constructor in a class, compiler automatically creates a default constructor
        The default constructor is used to provide the default values to the object like 0, null, etc., depending on the type
    */

    void displayTheValue() {
        System.out.println("Student reg no -> " + regNo);
        System.out.println("Student name -> " + name);
    }
}

class DefaultConstructor {
    public static void main(String[] args) {
        Student student = new Student();
        student.displayTheValue();
    }
}
