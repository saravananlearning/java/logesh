package ObjectConcept.ObjectCreation.OutsideTheClass;

class Student {
    int regNo;
    String name;
}

class OutsideTheClass {
    public static void main(String[] args) {
        Student student = new Student();
        System.out.println("Student reg no -> " + student.regNo);
        System.out.println("Student name -> " + student.name);

        student.regNo = 100;
        student.name = "Logesh";
        System.out.println("Student reg no -> " + student.regNo);
        System.out.println("Student name -> " + student.name);
    }

}
