package ObjectConcept.CopyObject.ByConstructor;

class Student {
    int regNo;
    String name;

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    Student(Student student) {
        regNo = student.regNo;
        name = student.name;
    }

    void displayTheValue() {
        System.out.println("Student reg no -> " + regNo);
        System.out.println("Student name -> " + name);
    }
}

class ByConstructor {
    public static void main(String[] args) {
        Student student1 = new Student(100, "Logesh");
        student1.displayTheValue();

        Student student2 = new Student(student1);
        student2.displayTheValue();
    }
}
