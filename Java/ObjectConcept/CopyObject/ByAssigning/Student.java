package ObjectConcept.CopyObject.ByAssigning;

class Student {
    int regNo;
    String name;

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    Student() { }

    void displayTheValue() {
        System.out.println("Student reg no -> " + regNo);
        System.out.println("Student name -> " + name);
    }
}

class ByAssigning {
    public static void main(String[] args) {
        Student student1 = new Student(100, "Logesh");
        student1.displayTheValue();

        Student student2 = new Student();
        student2.regNo = student1.regNo;
        student2.name = student1.name;
        student2.displayTheValue();
    }
}
