package ObjectConcept.ThisKeyword.PassAsArgumentInConstructor;

class B {
    A aa;
    int x;

    B() { }

    B(A a, int x) {
        this.aa = a;
        this.x = x;
    }

    void displayTheValue() {
        System.out.println(aa.data + " -> " + aa.hashCode() + " -> " + x);
    }
}

class A {
    int data = 10;

    A() {
        B b = new B(this, 25);
        b.displayTheValue();
        data = 100;
        b.displayTheValue();
    }
    public static void main(String[] args) {
        A a = new A();
        System.out.println(a.hashCode());
        // Student student = new Student(this); cann't use this in static context
    }
}
