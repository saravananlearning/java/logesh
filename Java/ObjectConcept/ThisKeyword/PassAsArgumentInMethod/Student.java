package ObjectConcept.ThisKeyword.PassAsArgumentInMethod;

class Student {
    int regNo;
    String name;

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    void displayTheValue() {
        displayTheStudentValue(this);
    }

    void displayTheStudentValue(Student s) {
        System.out.println(s.regNo + " -> " + s.name + " -> " + s.hashCode());
    }
}

class PassAsArgumentInMethod {
    public static void main(String[] args) {
        Student student = new Student(100, "Logesh");
        System.out.println(student.hashCode());
        student.displayTheValue();
        Student student2 = new Student(101, "Ranjith");
        System.out.println(student2.hashCode());
        student2.displayTheValue();
    }
}
