package ObjectConcept.ThisKeyword.ReferCurrentClassInstanceVariable;

class Student {
    int regNo;
    String name;
    String college;

    Student(int regNo, String name, String clg) {
        this.regNo = regNo;
        // name = name;  no effect - name refer the parameter value
        college = clg;
    }

    void displayTheValue() {
        System.out.println(regNo + " -> " + name + " -> " + college);
    }
}

class ReferCurrentClassInstanceVariable {
    public static void main(String[] args) {
        Student student = new Student(100, "Logesh", "MIT");

        student.displayTheValue();
    }
}
