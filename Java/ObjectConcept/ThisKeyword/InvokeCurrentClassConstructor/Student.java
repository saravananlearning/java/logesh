package ObjectConcept.ThisKeyword.InvokeCurrentClassConstructor;

class Student {
    int regNo;
    String name;
    String college;

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    Student(int regNo, String name, String college) {
        this(regNo, name);  // reusing the constructor && it must be first statement
        this.college = college;
    }

    void displayTheValue() {
        System.out.println(regNo + " -> " + name + " -> " + college);
    }
}

class InvokeCurrentClassConstructor {
    public static void main(String[] args) {
        Student student = new Student(100, "Logesh", "MIT");
        student.displayTheValue();
    }
}
