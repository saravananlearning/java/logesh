package ObjectConcept.ThisKeyword.InvokeCurrentClassMethod;

class Student {
    int regNo;
    String name;

    Student(int regNo, String name) {
        this.regNo = regNo;
        this.name = name;
    }

    void callingMethod() {
        // You may invoke the method of the current class by using the this keyword.
        // If you don't use the this keyword, compiler automatically adds this keyword while invoking the method
        displayTheValue();
    }

    void displayTheValue() {
        System.out.println(regNo + " -> " + name);
    }
}

class InvokeCurrentClassMethod {
    public static void main(String[] args) {
        Student student = new Student(100, "Logesh");
        student.callingMethod();
    }
}
