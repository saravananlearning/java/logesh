package ObjectConcept.ThisKeyword.ReturnCurrentClassInstance;

class Student {
    Student getStudentReferrence() {
        return this;
    }

    void message() {
        System.out.println("Student's message method is invoked");
    }

    public static void main(String[] args) {
        new Student().getStudentReferrence().message();
    }
}
