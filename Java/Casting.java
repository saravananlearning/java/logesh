class Casting {
    public static void main(String[] args) {
        System.out.println("## Casting");

        System.out.println("## Widening");
        int a = 10;
        float b = a;
        System.out.println("int to float: " + a + " -> " + b);

        System.out.println("## Narrowing(Typecasting)");

        float c = 10.5f;
        // int d = c; Type mismatch: cannot convert float to int
        int d = (int)c;
        System.out.println("float to int: " + c + " -> " + d);
    }
}
