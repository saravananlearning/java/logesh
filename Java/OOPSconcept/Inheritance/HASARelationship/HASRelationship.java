package OOPSconcept.Inheritance.HASARelationship;

class Student {
    int regNo = 100;
    String name = "Logesh";
    String gender = "Male";
    Address address;
}

class Address {
    String city = "chennai";
    String state = "Tamilnadu";
    String country = "India";
}

class HASRelationship {
    public static void main(String[] args) {
        Student student = new Student();
        Address address = new Address();
        student.address = address;
        System.out.println(student.regNo + " -> " + student.name + " -> " + student.gender);
        System.out.println(student.address.city + " -> " + student.address.state + " -> " + student.address.country);
    }
}

