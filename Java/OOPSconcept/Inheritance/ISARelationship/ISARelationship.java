package OOPSconcept.Inheritance.ISARelationship;

class Department {
    int deptNo = 1;
    String hod = "J Prakash";
}

class Student extends Department {
    int regNo = 100;
    String name = "Logesh";
    String gender = "Male";
    int deptNo = 2;
}

class ISARelationship {
    public static void main(String[] args) {
        Student student = new Student();
        System.out.println(
            student.deptNo + " -> " + student.hod + " -> " + 
            student.regNo + " -> " + student.name + " -> " + student.gender
        );

        Department department = new Department();
        System.out.println(
            department.deptNo + " -> " + department.hod
        );
    }
}
