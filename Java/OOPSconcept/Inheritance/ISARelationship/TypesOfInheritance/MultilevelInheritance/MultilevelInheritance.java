package OOPSconcept.Inheritance.ISARelationship.TypesOfInheritance.MultilevelInheritance;

class College {
    int clgId = 1;
    String clgName = "MIT";
}

class Department extends College {
    int deptNo = 1;
    String hod = "J Prakash";
}

class Student extends Department {
    int regNo = 100;
    String name = "Logesh";
    String gender = "Male";
    int deptNo = 1;
}

class SingleInheritance {
    public static void main(String[] args) {
        Student student = new Student();
        System.out.println(student.clgId + " -> " + student.clgName + " -> " +student.deptNo + " -> " + student.hod + " -> " + student.regNo + " -> " + student.name + " -> " + student.gender);
    }
}
