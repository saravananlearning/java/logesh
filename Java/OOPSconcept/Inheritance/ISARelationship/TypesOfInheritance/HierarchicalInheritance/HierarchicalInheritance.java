package OOPSconcept.Inheritance.ISARelationship.TypesOfInheritance.HierarchicalInheritance;


class Department {
    int deptNo = 1;
    String hod = "J Prakash";
}

class Student extends Department {
    int regNo = 100;
    String name = "Logesh";
    String gender = "Male";
    int deptNo = 1;
}

class Teacher extends Department {
    int regNo = 1000;
    String name = "Selva Kumar";
    String gender = "Male";
    int deptNo = 1;
}

class HierarchicalInheritance {
    public static void main(String[] args) {
        Student student = new Student();
        System.out.println(student.deptNo + " -> " + student.hod + " -> " + student.regNo + " -> " + student.name + " -> " + student.gender);
        Teacher teacher = new Teacher();
        System.out.println(teacher.deptNo + " -> " + teacher.hod + " -> " + teacher.regNo + " -> " + teacher.name + " -> " + teacher.gender);
    }
}

