package OOPSconcept.Polymorphism.MethodOverriding;

class Department {
    int deptNo = 1;
    String hod = "J Prakash";
    String location = "E&I Building";
    void getLocation() {
        System.out.println(location);
    }
}

class Student extends Department {
    int regNo = 100;
    String name = "Logesh";
    int deptNo = 1;
    String location = "Room No: 1, E&I Building";
    void getLocation() {
        System.out.println(location);
    }
}


class MethodOverriding {
    public static void main(String[] args) {
        Student student = new Student();
        student.getLocation();
    }
}
