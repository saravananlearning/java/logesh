package OOPSconcept.Polymorphism.RuntimePolymorphism;

class Department2 {
    String location = "E&I Building";
}

class Student2 extends Department2 {
    String location = "Room No: 1, E&I Building";
}


class RuntimePolymorphismVariable {
    public static void main(String[] args) {
        Department2 department = new Student2();
        System.out.println(department.location);
    }
}
