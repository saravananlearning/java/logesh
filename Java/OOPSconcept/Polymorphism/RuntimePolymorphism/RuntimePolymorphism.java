package OOPSconcept.Polymorphism.RuntimePolymorphism;

class Department {
    String location = "E&I Building";
    void getLocation() {
        System.out.println(location);
    }
}

class Student extends Department {
    String location = "Room No: 1, E&I Building";
    void getLocation() {
        System.out.println(location);
    }
}


class RuntimePolymorphism {
    public static void main(String[] args) {
        Department department = new Student();
        department.getLocation();
    }
}
