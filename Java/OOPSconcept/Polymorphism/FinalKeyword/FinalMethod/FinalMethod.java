package OOPSconcept.Polymorphism.FinalKeyword.FinalMethod;

class Department {
    String deptName;

    final void setDeptName(String deptName) {
        this.deptName = deptName;
    }
}

class Student extends Department {
    // void setDeptName(String deptName) {
    //     this.deptName = deptName;
    // }
    void getDetpName() {
        System.out.println(deptName);
    }
}


class FinalMethod {
    public static void main(String[] args) {
        Student student = new Student();
        student.setDeptName("E&I");
        student.getDetpName();
    }
}
