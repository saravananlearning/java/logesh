package OOPSconcept.Polymorphism.FinalKeyword.FinalVariable;

class Student {
    final int regNo = 100;

    void changeRegNo() {
        // regNo = 200; // Final variable value can never be changed once it is assigned
    }

    void printRegNo() {
        System.out.println(regNo);
    }
}

class FinalVariable {
    public static void main(String[] args) {
        Student student = new Student();
        student.printRegNo();
        student.changeRegNo();
        student.printRegNo();
    }
}
