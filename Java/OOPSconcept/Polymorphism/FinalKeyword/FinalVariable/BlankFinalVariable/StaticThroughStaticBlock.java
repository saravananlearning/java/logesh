package OOPSconcept.Polymorphism.FinalKeyword.FinalVariable.BlankFinalVariable;

class Student2 {
    final static int regNo;

    static { regNo = 100; }

    void printRegNo() {
        System.out.println(regNo);
    }
}

class StaticThroughStaticBlock {
    public static void main(String[] args) {
        Student2 student = new Student2();
        student.printRegNo();
    }
}
