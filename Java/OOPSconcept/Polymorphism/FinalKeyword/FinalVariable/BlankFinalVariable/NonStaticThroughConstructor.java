package OOPSconcept.Polymorphism.FinalKeyword.FinalVariable.BlankFinalVariable;

class Student {
    final int regNo;

    Student(int regNo) {
        this.regNo = regNo;
    }

    void printRegNo() {
        System.out.println(regNo);
    }
}

class NonStaticThroughConstructor {
    public static void main(String[] args) {
        Student student = new Student(100);
        student.printRegNo();
    }
}
