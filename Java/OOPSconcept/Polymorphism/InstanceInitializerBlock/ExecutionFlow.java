package OOPSconcept.Polymorphism.InstanceInitializerBlock;

class Parent {
    Parent() {
        System.out.println("Parent class constructor is invoked");
    }
}

class Child extends Parent {
    { System.out.println("Instance Initializer block is invoked"); }
    { System.out.println("Instance Initializer block 2 is invoked"); }

    Child() {
        super();
        System.out.println("Child class constructor is invoked");
    }

    void childMethod() {
        System.out.println("Child method is invoked when it is called");
    }
}

class ExecutionFlow {
    public static void main(String[] args) {
        Child child = new Child();
        child.childMethod();
    }
}
