package OOPSconcept.Polymorphism.InstanceInitializerBlock;

class Student {
    String name;
    String gender;

    { gender = "Male"; }

    Student(String name) {
        this.name = name;
    }

    void displayTheValue() {
        System.out.println(name + " -> " + gender);
    }
}


class InstanceInitializerBlock {
    public static void main(String[] args) {
        Student student = new Student("Logesh");
        student.displayTheValue();
    }
}
