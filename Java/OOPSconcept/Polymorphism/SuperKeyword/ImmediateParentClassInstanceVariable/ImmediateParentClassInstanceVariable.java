package OOPSconcept.Polymorphism.SuperKeyword.ImmediateParentClassInstanceVariable;

class Department {
    int deptNo = 1;
    String hod = "J Prakash";
    String location = "E&I Building";
}

class Student extends Department {
    int regNo = 100;
    String name = "Logesh";
    int deptNo = 1;
    String location = "Room No: 1, E&I Building";
    void getLocation() {
        System.out.println(super.location);
        System.out.println(location);
    }
}


class ImmediateParentClassInstanceVariable {
    public static void main(String[] args) {
        Student student = new Student();
        student.getLocation();
    }
}
