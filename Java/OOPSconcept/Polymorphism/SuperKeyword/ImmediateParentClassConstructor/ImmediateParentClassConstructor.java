package OOPSconcept.Polymorphism.SuperKeyword.ImmediateParentClassConstructor;

class Person {
    String name;
    String gender;

    Person(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }
}

class Employee extends Person {
    int regNo;
    String dept;
    int salary;

    Employee(String name, String gender, int regNo, String dept, int salary) {
        super(name, gender);
        this.regNo = regNo;
        this.dept = dept;
        this.salary = salary;
    }

    void displayTheValue() {
        System.out.println(regNo + " -> " + dept + " -> " + salary);
        System.out.println(name + " -> " + gender);
    }
}


class ImmediateParentClassConstructor {
    public static void main(String[] args) {
        Employee employee = new Employee("Logesh", "Male", 100, "Software Engineer", 20000);
        employee.displayTheValue();
    }
}
