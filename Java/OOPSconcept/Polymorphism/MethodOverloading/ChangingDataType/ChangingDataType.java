package OOPSconcept.Polymorphism.MethodOverloading.ChangingDataType;


class Student {
    static String userName;

    static void setUserName(String firstName, String lastName) {
        userName = firstName + lastName;
    }

    static void setUserName(String firstName, int regNo) {
        userName = firstName + regNo;
    }

    void displayTheValue() {
        System.out.println(userName);
    }
}


class ChangingDataType {
    public static void main(String[] args) {
        Student student = new Student();
        Student.setUserName("Logesh", "waran");
        student.displayTheValue();

        Student student2 = new Student();
        Student.setUserName("Logesh", 100);
        student.displayTheValue();
        student2.displayTheValue();
    }
}
