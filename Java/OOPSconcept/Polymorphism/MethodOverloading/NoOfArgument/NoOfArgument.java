package OOPSconcept.Polymorphism.MethodOverloading.NoOfArgument;

class Student {
    String userName;

    void getUserName(String name, int regNo) {
        userName = name + regNo;
    }

    void getUserName(String name) {
        userName = name;
    }

    void displayTheValue() {
        System.out.println(userName);
    }
}

class NoOfArgument {
    public static void main(String[] args) {
        Student student = new Student();
        student.getUserName("Logesh");
        student.displayTheValue();

        Student student2 = new Student();
        student2.getUserName("Logesh", 100);
        student2.displayTheValue();
    }
    
}
