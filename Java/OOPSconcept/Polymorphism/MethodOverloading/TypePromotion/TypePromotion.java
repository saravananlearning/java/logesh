package OOPSconcept.Polymorphism.MethodOverloading.TypePromotion;

class Student {
    String userName;

    void setUserName(String name, float regNo) {
        userName = name + regNo;
    }

    void displayTheValue() {
        System.out.println(userName);
    }
}


class TypePromotion {
    public static void main(String[] args) {
        Student student = new Student();
        // One type is promoted to another implicitly if no matching datatype is found.
        // int -> long -> float -> double
        student.setUserName("Logesh", 100); 
        student.displayTheValue();
    }
}
