import java.util.Scanner;

class PrimeNumber {
    public static void main(String[] args) {
        System.out.println("Prime Number");

        System.out.println("Enter a number");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        if(isPrimeNumber(number)) {
            System.out.println("Given number is prime number");
        } else {
            System.out.println("Given number is not a prime number");
        }

        scanner.close();
    }

    static boolean isPrimeNumber(int n) {
        if(n==0 || n==1) {
            return false;
        } else {
            for(int i=2; i<n; i++) {
                if(n % i == 0) {
                    return false;
                }
            }
        }
        return true;
    }
}
