package Collections.List.ArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class ArrayListMethods3 {
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<String>();
        al.add("yuvi");
        al.add("logi");
        al.add("ranjith");
        al.add("logi");
        al.add("surya");
        al.add("veda");
        al.add("gopi");
        System.out.println(al);

        System.out.println("---------- size method ----------");
        System.out.println(al.size());

        System.out.println("---------- indexOf method ----------");
        int eleIndex = al.indexOf("logi");
        System.out.println(eleIndex);

        System.out.println("---------- lastIndexOf method ----------");
        int eleLastIndex = al.lastIndexOf("logi");
        System.out.println(eleLastIndex);

        System.out.println("---------- Iteration ways ----------");

        System.out.println("---------- forEach method ----------");
        al.forEach(val -> {
            System.out.print(val + " ");
        });
        System.out.println();

        System.out.println("---------- iterator method ----------");
        Iterator<String> itr = al.iterator();
        itr.forEachRemaining(val -> {
            System.out.print(val + " ");
        });
        System.out.println();

        System.out.println("---------- listIterator method ----------");
        ListIterator<String> list =  al.listIterator();
        while(list.hasNext()) {
            String val = list.next();
            System.out.print(val + " ");
        }
        System.out.println();

        ListIterator<String> list1 = al.listIterator(3);
        while(list1.hasNext()) {
            String val = list1.next();
            System.out.print(val + " ");
        }
        System.out.println();
    }
}
