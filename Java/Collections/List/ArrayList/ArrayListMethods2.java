package Collections.List.ArrayList;

import java.util.ArrayList;

public class ArrayListMethods2 {
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<String>();
        al.add("yuvi");
        al.add("logi");
        al.add("ranjith");

        // Check whether the given element is present or not
        System.out.println(al.contains("logi"));
        System.out.println(al.contains("logesh"));
        System.out.println("--------------------");

        ArrayList<String> al2 = new ArrayList<String>();
        al2.add("yuvi");
        al2.add("logi");
        al2.add("ranjith");

        ArrayList<String> al3 = new ArrayList<String>();
        al3.add("logi");
        al3.add("yuvi");
        al3.add("ranjith");

        ArrayList<String> al4 = new ArrayList<String>();
        al4.add("logi");
        al4.add("yuvi");
        al4.add("ranjith");
        al4.add("logi");
        al4.add("ranjith");

        ArrayList<String> al5 = new ArrayList<String>();
        al5.add("yuvi");
        al5.add("padma");
        al5.add("ranjith");

        // Check whether the given all elements of the collection is present or not
        System.out.println(al.containsAll(al2));
        System.out.println(al.containsAll(al3));
        System.out.println(al.containsAll(al4));
        System.out.println(al.containsAll(al5));
        System.out.println("####################");

        // Same size, Same order and element
        System.out.println(al.equals(al2));
        System.out.println(al.equals(al3));
        System.out.println(al.equals(al4));
        System.out.println(al.equals(al5));
    }
}
