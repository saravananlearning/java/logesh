package Collections.List.ArrayList;

import java.util.ArrayList;
import java.util.List;

public class ArrayListMethods {
    public static void main(String[] args) {
        // To create an empty array list
        ArrayList<String> al = new ArrayList<String>();
        System.out.println(al);

        System.out.println("---------- add method ----------");
        // To add element at the end of the list
        al.add("logi");
        al.add("ranjith");
        System.out.println(al);

        // To add element at the specified postion in the list
        al.add(0, "yuvi");
        System.out.println(al);
        System.out.println("####################");

        ArrayList<String> al2 = new ArrayList<String>();
        al2.add("surya");
        al2.add("veda");
        al2.add("gopi");
        System.out.println(al2);
        ArrayList<String> al3 = new ArrayList<String>();
        al3.add("sarath");
        al3.add("partha");
        al3.add("dhana");
        System.out.println(al3);
        System.out.println("---------- addAll method ----------");

        // To add collection at the end of the list
        al.addAll(al2);
        System.out.println(al);

        // To add collection at the specified postion in the list
        al.addAll(3, al3);
        System.out.println(al);

        System.out.println("---------- remove method ----------");
        al.remove(0);
        System.out.println(al);

        al.remove("logi");
        System.out.println(al);

        System.out.println("---------- removeAll method ----------");
        al.removeAll(al2);
        System.out.println(al);
        System.out.println("####################");

        al.add("ranjith");
        al.add(2, "ranjith");
        System.out.println(al);

        System.out.println("---------- removeIf method ----------");
        al.removeIf(str -> str.contains("ranjith"));
        System.out.println(al);

        System.out.println("---------- get method ----------");
        String get2ndElement = al.get(1);
        System.out.println(get2ndElement);

        System.out.println("---------- set method ----------");
        al.set(1, "parthiban");
        System.out.println(al);

        System.out.println("---------- subList method ----------");
        List<String> sublist = al.subList(1, 3);
        System.out.println(sublist);

        ArrayList<String> al4 = new ArrayList<String>();
        al4.add("parthiban");
        al4.add("dhana");

        System.out.println("---------- retainAll method ----------");
        al.retainAll(al4);
        System.out.println(al);

        System.out.println("---------- replaceAll method ----------");
        al.replaceAll(e -> e.toUpperCase());
        System.out.println(al);

        // Object cal =  al.clone();
        // System.out.println(cal);

        // To remove all elements in the list
        al.clear();
        System.out.println(al.size() + " -> " + al);
    }
}