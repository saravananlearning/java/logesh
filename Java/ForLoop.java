class ForLoop {
    public static void main(String[] args) {
        System.out.println("For loop");

        for(int i=0; i<10; i++) {
            System.out.println("Iteration: "+ (i + 1));
        }

        System.out.println("Nested For loop");
        for(int i=0; i<3; i++) {
            for(int j=0; j<3; j++) {
                System.out.print(i + "" + j + " ");
            }
            System.out.println();
        }

        System.out.println("for-each loop");
        int arrVar[]={1, 2, 3, 4, 5};
        for(int i: arrVar) {
            System.out.print(i + " ");
        }

    }
}