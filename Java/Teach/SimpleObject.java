package Teach;

class Student {
    int rollNo = 1;
    String name = "Logesh";
    String gender = "Male";
    boolean isFeePaid = false;
    char section = 'A';
    String[] subject = { "Maths", "Tamil", "Science" };

    void feePaid() {
        isFeePaid = true;
    }
}

public class SimpleObject {
    public static void main(String[] args) {
        // object creation
        Student s = new Student();
        System.out.println(s.rollNo + " -> " + s.gender + " -> " + s.name+ " -> " + s.isFeePaid + " -> " + s.section);
    }
}
