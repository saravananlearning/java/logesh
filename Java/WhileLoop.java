class WhileLoop {
    public static void main(String[] args) {
        System.out.println("While loop");
        int i = 0;
        while(i < 10) {
            System.out.println("Iteration: " + (i + 1));
            i++;
        }
    }
}
