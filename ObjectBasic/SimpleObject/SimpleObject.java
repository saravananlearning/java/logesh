package logesh.ObjectBasic.SimpleObject;

class Student {
    int rollNo = 1;
    String name = "Logesh";
    String gender = "Male";
    boolean isFeePaid = false;
    char section = 'A';
    String[] subject = { "Maths", "Tamil", "Science" };
}

public class SimpleObject {
    public static void main(String[] args) {
        // object creation
        Student s = new Student();
        // access the object variable
        System.out.println(s.rollNo + " -> " + s.gender + " -> " + s.name+ " -> " + s.isFeePaid + " -> " + s.section);
    }
}
