package logesh.ObjectBasic.FeePaid;

class Student {
    int rollNo = 1;
    String name = "Logesh";
    String gender = "Male";
    boolean isFeePaid = false;
    char section = 'A';
    String[] subject = { "Maths", "Tamil", "Science" };

    void feePaid() {
        isFeePaid = true;
    }
}

public class FeePaid {
    public static void main(String[] args) {
        // object creation
        Student s1 = new Student();
        Student s2 = new Student();
        System.out.println("Today");
        System.out.println(s1.rollNo + " -> " + s1.gender + " -> " + s1.name+ " -> " + s1.isFeePaid + " -> " + s1.section);
        System.out.println(s2.rollNo + " -> " + s2.gender + " -> " + s2.name+ " -> " + s2.isFeePaid + " -> " + s2.section);

        System.out.println("After 10 days from today");
        s2.feePaid();
        System.out.println(s1.rollNo + " -> " + s1.gender + " -> " + s1.name+ " -> " + s1.isFeePaid + " -> " + s1.section);
        System.out.println(s2.rollNo + " -> " + s2.gender + " -> " + s2.name+ " -> " + s2.isFeePaid + " -> " + s2.section);

    }
}

