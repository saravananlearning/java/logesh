package logesh.ObjectBasic.ClassConstructor;

class Student {
    int rollNo;
    String name;
    boolean isFeePaid;

    Student(int r, String n, boolean f) {
        rollNo = r;
        name = n;
        isFeePaid = f;
    }

}


public class ClassConstructor {
    public static void main(String[] args) {
        Student student = new Student(100, "Logesh", false);
        System.out.println(student.rollNo + " -> " + student.name + " -> " + student.isFeePaid);
    }
}
