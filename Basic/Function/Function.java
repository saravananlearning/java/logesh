package logesh.Basic.Function;

public class Function {
    public static void main(String[] args) {
        function1();
        int returnValue = function2();
        System.out.println(returnValue);
        String returnValue3 = function3();
        System.out.println(returnValue3);
    }

    static void function1() {
        System.out.println("void function");
    }

    static int function2() {
        System.out.println("int function");
        return 5;
    }

    static String function3() {
        System.out.println("String function");
        return "logesh";
    }

}
